import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import App from './app.vue';

Vue.use(VueRouter);
Vue.use(Vuex);

const Routers = [{
        path: '/',
        meta: {
            title: '首页'
        },
        component: (resolve) => require(['./views/index.vue'], resolve)
    },
    {
        path: '/index',
        meta: {
            title: '首页'
        },
        component: (resolve) => require(['./views/index.vue'], resolve)
    },
    {
        path: '/about',
        meta: {
            title: '关于'
        },
        component: (resolve) => require(['./views/about.vue'], resolve)
    },
    {
        path: '/user/:id',
        component: (resolve) => require(['./views/user.vue'], resolve)
    },
    {
        path: '*',
        meta: {
            title: '个人主页'
        },
        redirect: '/index'
    }
];

const RouterConfig = {
    // 使用HTML5的History路由模式
    mode: 'history',
    routes: Routers
};
const router = new VueRouter(RouterConfig);
router.beforeEach((to, from, next) => {
    window.document.title = to.meta.title;
    // if (window.localStorage.getItem('token')) {
    next();
    // } else {
    // next('/login');
    // }

})
router.afterEach((to, from, next) => {
    window.scrollTo(0, 0);
})

const store = new Vuex.Store({
    state: {
        count: 0
    },
    mutations: {
        increment(state) {
            state.count++;
        },
        decrease(state) {
            state.count--;
        }
    }
});

var app = new Vue({
    el: '#app',
    router: router,
    store: store,
    render: h => {
        return h(App)
    }
});